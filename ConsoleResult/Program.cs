﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaxHelpers.EncryptHelper;
namespace ConsoleResult
{
    //这里是测试用的控制台，用来调用方法看是否成功。
    class Program
    {
        static void Main(string[] args)
        {
           
            string strTest = "123456";
            string m1Up = MD5Encrypt.Encrypt(strTest);
            string m1Low = MD5Encrypt.Encrypt(strTest, false);
            //16位
            string m2Up = MD5Encrypt.Encrypt(strTest, true, 16);
            string m2Low = MD5Encrypt.Encrypt(strTest, false, 16);

            OutputAndLength(m1Up);
            OutputAndLength(m1Low);
            OutputAndLength(m2Up);
            OutputAndLength(m2Low);

            //两个相同的文件，文件名字不同用作对比

            string strPath = @"文件路径";
            string strPath2 = @"文件路径2";

            string fmd51 = MD5Encrypt.AbstractFile(strPath);
            string fmd52 = MD5Encrypt.AbstractFile(strPath2);
            OutputAndLength(fmd51);
            OutputAndLength(fmd52);
            Console.ReadLine();
        }
        static void OutputAndLength(string str)
        {
            Console.WriteLine($"输入字符串:{str},长度:{str.Length}");
        }
    }
}
