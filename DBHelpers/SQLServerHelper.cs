﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography.X509Certificates;

namespace MaxHelpers.DBHelpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class SQLServerHelper
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public static readonly string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SqlText">SQL语句</param>
        /// <returns>受影响的记录数</returns>
        public static int ExecuteSQLNonQuery(String SqlText)
        {
            if (SqlText == string.Empty && SqlText == null)
            {
                throw new ArgumentNullException("SqlText");
            }
            int result = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = SqlText;
                        connection.Open();
                        result = command.ExecuteNonQuery();
                        return result;
                    }
                    catch (SqlException sqle)
                    {
                        connection.Close();
                        throw sqle;
                    }
                    
                }
            }
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SqlText">SQL语句</param>
        /// <param name="parameters">SQL语句的参数集合</param>
        /// <returns></returns>
        public static int ExecuteSQLNonQuery(String SqlText, params SqlParameter[] parameters)
        {
            if (SqlText == string.Empty && SqlText == null)
            {
                throw new ArgumentNullException("SqlText");
            }
            if (parameters.Length==0||parameters==null)
            {
                return ExecuteSQLNonQuery(SqlText);
            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.CommandText = SqlText;
                        command.Connection = connection;
                        command.Parameters.AddRange(parameters);
                        int result = command.ExecuteNonQuery();
                        return result;
                    }
                    catch (SqlException sqle)
                    {
                        connection.Close();
                        throw sqle;
                    }
                   
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回单个查询结果 
        /// </summary>
        /// <param name="SqlText">SQL语句</param>
        /// <returns>单个查询结果,Object类型</returns>
        public static Object ExecuteToGetScalar(String SqlText)
        {
            if (SqlText == string.Empty && SqlText == null)
            {
                throw new ArgumentNullException("SqlText");
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = SqlText;
                        connection.Open();
                        Object result = command.ExecuteScalar();
                        if ((Object.Equals(result, null)) || (Object.Equals(result, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return result;
                        }
                    }
                    catch (SqlException sqle)
                    {
                        connection.Close();
                        throw sqle;
                    }
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回单个查询结果 
        /// </summary>
        /// <param name="SqlText">SQL语句</param>
        /// <param name="parameters">SQL语句的参数集合</param>
        /// <returns></returns>
        public static Object ExecuteToGetScalar(String SqlText, params SqlParameter[] parameters)
        {
            if (SqlText == string.Empty && SqlText == null)
            {
                throw new ArgumentNullException("SqlText");
            }
            if (parameters.Length == 0 || parameters == null)
            {
                return ExecuteToGetScalar(SqlText);
            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = SqlText;
                        command.Parameters.AddRange(parameters);
                        connection.Open();
                        Object result = command.ExecuteScalar();
                        if ((Object.Equals(result, null)) || (Object.Equals(result, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return result;
                        }
                    }
                    catch (SqlException sqle)
                    {
                        connection.Close();
                        throw sqle;
                    }
                }
            }
        }

        /// <summary>
        /// 跟据Sql语句获取SqlDataReader
        /// 建议使用using语句调用，避免忘记关闭SqlDataReader
        /// </summary>
        /// <param name="SqlText">SQL语句</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteToGetReader(String SqlText)
        {
            if (SqlText == string.Empty && SqlText == null)
            {
                throw new ArgumentNullException("SqlText");
            }
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command.CommandText = SqlText;
                connection.Open();
                return command.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (SqlException ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }
        }

        /// <summary>
        /// 跟据Sql语句获取SqlDataReader
        /// 建议使用using语句调用，避免忘记关闭SqlDataReader
        /// </summary>
        /// <param name="SqlText">SQL语句</param>
        /// <param name="parameters">SQL语句的参数集合</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteToGetReader(String SqlText, params SqlParameter[] parameters)
        {
            if (SqlText == string.Empty && SqlText == null)
            {
                throw new ArgumentNullException("SqlText");
            }
            if (parameters.Length == 0 || parameters == null)
            {
                return ExecuteToGetReader(SqlText);
            }
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                command.CommandText = SqlText;
                command.Parameters.AddRange(parameters);
                connection.Open();
                return command.ExecuteReader(CommandBehavior.CloseConnection);
                
            }
            catch (SqlException ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }
        }

        #region 存储过程

        #endregion

      

       
    }
}
