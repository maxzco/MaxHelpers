﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace MaxHelpers.EncryptHelper
{
    /// <summary>
    /// MD5 加密工具类
    /// </summary>
    public abstract class MD5Encrypt
    {
        private static readonly string UperX = "X2";
       
        /// <summary>
        /// MD5摘要
        /// </summary>
        /// <param name="source">需要加密的字符串</param>
        /// <param name="isUpper">是否大写，默认大写</param>
        /// <param name="length">需要返回的长度，只能是32或者16，默认32</param>
        /// <param name="encodeName">编码名称，默认UTF8</param>
        /// <returns></returns>
        public static string Encrypt(string source, bool isUpper = true, int length = 32, string encodeName="utf-8")
        {
            string EncryptedStr = string.Empty;
            if (string.IsNullOrEmpty(source))
            {
                throw new Exception("source字符串为空");
            }
            if (length!=32&&length!=16)
            {
                throw new Exception("length的值只能是16或32");
            }
            MD5 md5 = new MD5CryptoServiceProvider();
           
            byte[] fromData = Encoding.GetEncoding(encodeName).GetBytes(source);
            byte[] targetData = md5.ComputeHash(fromData);

            if (length==32)
            {
                //生成32位
                StringBuilder encryptedStringBuilder = new StringBuilder();
                for (int i = 0; i < targetData.Length; i++)
                {
                    encryptedStringBuilder.Append(targetData[i].ToString(UperX));
                }
                EncryptedStr = encryptedStringBuilder.ToString();
            }
            else if (length==16)
            {
                //生成16位
                EncryptedStr = BitConverter.ToString(targetData, 4, 8);
                EncryptedStr = EncryptedStr.Replace("-", "");
            }
            if (!isUpper)
            {
                EncryptedStr = EncryptedStr.ToLower();
            }
            return EncryptedStr;
        }

        /// <summary>
        /// 生成文件的MD5摘要 32位大写字符串
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static string AbstractFile(string filepath)
        {
            using (FileStream fileStream = new FileStream(filepath, FileMode.Open))
            {
                return AbstractFile(fileStream);
            }
        }

        /// <summary>
        /// 文件MD5摘要
        /// </summary>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        private static string AbstractFile(FileStream fileStream)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] targetData = md5.ComputeHash(fileStream);
            fileStream.Close();
            StringBuilder encryptedStringBuilder = new StringBuilder();
            for (int i = 0; i < targetData.Length; i++)
            {
                encryptedStringBuilder.Append(targetData[i].ToString(UperX));
            }
            return encryptedStringBuilder.ToString();
        }
    }
}
