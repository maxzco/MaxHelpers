﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxHelpers.StringHelper
{
    /// <summary>
    /// String 字符串的扩展小方法
    /// </summary>
    public static class StrPieces
    {
        /// <summary>
        /// 字符串转16进制
        /// </summary>
        /// <param name="s">源字符串</param>
        /// <param name="charset">编码格式名称,默认utf-8</param>
        /// <returns>源字符串的16进制</returns>
        public static string StringToHex(this string s, string charset = "utf-8")
        {
            Encoding chs = Encoding.GetEncoding(charset);
            byte[] bytes = chs.GetBytes(s);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                //把byte转化成十六进制string加到stringBuilder
                stringBuilder.Append(Convert.ToString(bytes[i], 16));
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// 16进制转字符串
        /// </summary>
        /// <param name="s">16进制字符串</param>
        /// <param name="charset">编码格式名称,默认utf-8</param>
        /// <returns>16进制转字符串</returns>
        public static string HexToString(this string s, string charset = "utf-8")
        {
            StringBuilder stringBuilder = new StringBuilder();
            //16进制字符串转byte数组
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
            {
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            }
            return Encoding.GetEncoding(charset).GetString(buffer);
        }
    }
}
